# OKADA HSR Common
OKADA LAB. development HSR common utilities.

- Ubuntu 20.04
- ROS Noetic

## Getting started
```
$ mkdir -p ~/catkin_ws/src
$ cd ~/catkin_ws/src
$ catkin_init_workspace
$ git clone https://gitlab.com/tidbots/HSR/okd_hsr_common.git
$ cd okd_hsr_common
$ rosdep install -y -r --from-path . --ignore-src
$ cd ~/catkin_ws
$ catkin_make
```

## How to use
```
$ source ~/catkin_ws/devel/setup.bash
$ rosrun 
```

