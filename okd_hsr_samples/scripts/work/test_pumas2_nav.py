#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import rospy
import smach
import smach_ros
import tf2_ros
import math
import numpy as np
import random
import traceback
import types
from contextlib import ExitStack


import hsrb_interface
from tf.transformations import quaternion_matrix, quaternion_from_euler, quaternion_from_matrix

from hsrb_interface import geometry
from geometry_msgs.msg import Vector3Stamped, Pose, PoseStamped, PointStamped
from geometry_msgs.msg import WrenchStamped, Twist
import actionlib
from actionlib_msgs.msg import GoalStatus
from geometry_msgs.msg import Point, PoseStamped, Quaternion
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
import rospy
import tf.transformations

#from common import speech
#from common.smach_states import *
#from common import rospose_to_tmcpose

from takeshi_tools.nav_tool_lib import nav_module
##################################################
import rospy
import json
import urllib
from datetime import datetime
import time

# Main
robot = hsrb_interface.Robot()

whole_body = robot.get("whole_body")
#omni_base = robot.get("omni_base")
omni_base = nav_module("pumas")  # New initalisation (Pumas)
#tf_buffer = robot._get_tf2_buffer()


whole_body.move_to_neutral()
rospy.loginfo('initializing...')
rospy.sleep(1)


def create_sm():
    sm = smach.StateMachine(outcomes=['success', 'failure', 'timeout'])
    with sm:
        @smach.cb_interface(outcomes=['success', 'failure', 'timeout'])
        def test_state_cb(self):
            try:
       	        omni_base.go_abs(0.0, 0.0, 3.0, 0)
                return 'success'
            except:
                return 'failure'
        smach.StateMachine.add('TESTSTATE', smach.CBState(test_state_cb),
				transitions = {'success': 'success',
				'failure': 'failure','timeout':'timeout'})

 ##########
	#END: BASIC STATE FUNCTION
	##########

    return sm

sm = create_sm()
outcome = sm.execute()

if outcome == 'success':
    rospy.loginfo('I finished the task.')
else:
    rospy.signal_shutdown('Some error occured.')
