#!/usr/bin/env python
# -*- coding: utf-8 -*-

import rospy
import roslaunch

from std_srvs.srv import SetBool, SetBoolResponse

is_launch = True
is_stop = False

def callback_srv(data):
    global is_stop
    resp = SetBoolResponse()
    if data.data == True:
        is_stop = True
        resp.message = "stop"
        resp.success = True

    else:
        is_stop = False
        resp.message = "continue"
        resp.success = False

    print(resp.message)
    return resp

class ProcessListener(roslaunch.pmon.ProcessListener):
    global is_launch
    def process_died(self, name, exit_code):
	global is_launch
        is_launch = False
        rospy.logwarn("%s died with code %s", name, exit_code)

def init_launch(launchfile, process_listener):
    uuid = roslaunch.rlutil.get_or_generate_uuid(None, False)
    roslaunch.configure_logging(uuid)
    launch = roslaunch.parent.ROSLaunchParent(
        uuid,
        launchfile,
        process_listeners=[process_listener],
    )
    return launch

if __name__ == "__main__":
    rospy.init_node("launch_launcher", anonymous=True)

    #cli_args = ['pkg', 'file1.launch', 'arg1:=arg1', 'arg2:=arg2']
    cli_args1 = ['jpop2019','erasersvision.launch']

    srv = rospy.Service('vision_launch_call', SetBool, callback_srv)

    roslaunch_file1 = roslaunch.rlutil.resolve_launch_arguments(cli_args1)[0]
    roslaunch_args1 = cli_args1[2:]

    launch_files = [(roslaunch_file1, roslaunch_args1)]

    launch = init_launch(launch_files, ProcessListener())
    launch.start()

    try:
	    while is_launch and not is_stop and not rospy.is_shutdown():
		rospy.sleep(1)
		#rospy.spin()
    except:
	    print("Terminated by user")

    launch.shutdown()
    rospy.signal_shutdown('task finished')
