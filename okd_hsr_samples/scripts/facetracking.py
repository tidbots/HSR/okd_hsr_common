#!/usr/bin/env python3
## coding: UTF-8
# Copyright (c) 2023 Hiroyuki Okada
# This software is released under the MIT License.
# http://opensource.org/licenses/mit-license.php
"""
顔を検出してその方向にHSRの頭を向ける
roscore
rosrun usb_cam usb_cam_node _video_device:=/dev/video2
roslaunch opencv_apps face_detection.launch image:=/usb_cam/image_raw
rosrun okd_hsrsample facetracking.py
"""
import math
import sys
import rospy
import actionlib
import control_msgs.msg
import controller_manager_msgs.srv
import trajectory_msgs.msg
from trajectory_msgs.msg import JointTrajectoryPoint

# for face tracker
from sensor_msgs.msg import Image
from opencv_apps.msg import FaceArrayStamped
from geometry_msgs.msg import Point
IMAGE_WIDTH = 640
IMAGE_HEIGHT = 480

# for Head PanTilt
import actionlib
from control_msgs.msg import (
    FollowJointTrajectoryAction,
    FollowJointTrajectoryGoal,
    JointTrajectoryControllerState
)


class FaceTracker:
    def __init__(self):
        self.face_detected = False
        self.face_pos = Point()

        # Subscriberを作成
        self.sub_face =  rospy.Subscriber('/face_detection/faces', FaceArrayStamped, self.callback)
        # Wait for connection
        while self.sub_face.get_num_connections() == 0:
            rospy.sleep(0.1)


    def callback(self, faces):
        if (len(faces.faces)) <=0:
            self.face_detected = False
            return

        self.face_detected = True

        # 認識された顔が大きい順に並び替える
        faces.faces.sort(key=lambda f: f.face.width * f.face.height)

        # 画像中心を0, 0とした座標系における顔の座標を出力
        # 顔の座標は-1.0 ~ 1.0に正規化するn
        self.face_pos.x = faces.faces[0].face.x/320 -1.0
        self.face_pos.y = (faces.faces[0].face.y/240 -1.0)*-1.0

    def face_detect(self):
        return self.face_detected

    def get_face_position(self):
        return self.face_pos

class OmniBase(object):
    def __init__(self):
        # initialize action client
        self.__client = actionlib.SimpleActionClient(
        '/hsrb/omni_base_controller/follow_joint_trajectory',
            control_msgs.msg.FollowJointTrajectoryAction)

        # wait for the action server to establish connection
        self.__client.wait_for_server()

        # make sure the controller is running
        rospy.wait_for_service('/hsrb/controller_manager/list_controllers')
        list_controllers = rospy.ServiceProxy(
         '/hsrb/controller_manager/list_controllers',
           controller_manager_msgs.srv.ListControllers)
        running = False
        while running is False:
            rospy.sleep(0.1)
            for c in list_controllers().controller:
                if c.name == 'omni_base_controller' and c.state == 'running':
                    running = True

        self._state_sub = rospy.Subscriber("/hsrb/omni_base_controller/state",
                JointTrajectoryControllerState, self._state_callback, queue_size=1)

        self._state_received = False
        self._current_omni = 0.0 # Degree
        self._current_x = 0.0
        self._current_y = 0.0

    def _state_callback(self, state):
        # 首の現在角度を取得
        self._state_received = True
        omni_radian = state.actual.positions[2]

        self._current_omni = math.degrees(omni_radian)
        self._current_x = math.degrees(state.actual.positions[0])
        self._current_y = math.degrees(state.actual.positions[1])

    def state_received(self):
        return self._state_received

    def get_current_x(self):
        return self._current_x # Radian
    def get_current_y(self):
        return self._current_y # Radian        
    def get_current_omni(self):
        return self._current_omni # Radian

    def set_omni(self, omni_angle, goal_secs=1.0e-9):
        # 台車を指定角度に動かす
        goal = FollowJointTrajectoryGoal()
        goal.trajectory.joint_names = ["odom_x", "odom_y", "odom_t"]

        panpoint = JointTrajectoryPoint()
        panpoint.positions.append(self.get_current_x())
        panpoint.positions.append(self.get_current_y())
        panpoint.positions.append(omni_angle)
        panpoint.time_from_start = rospy.Duration(goal_secs)
        goal.trajectory.points.append(panpoint)

        self.__client.send_goal(goal)
        self.__client.wait_for_result(rospy.Duration(0.1))
        return self.__client.get_result()



class HeadPanTilt(object):
    def __init__(self):
        # initialize action client
        self.__client = actionlib.SimpleActionClient(
        '/hsrb/head_trajectory_controller/follow_joint_trajectory',
            control_msgs.msg.FollowJointTrajectoryAction)

        # wait for the action server to establish connection
        self.__client.wait_for_server()

        # make sure the controller is running
        rospy.wait_for_service('/hsrb/controller_manager/list_controllers')
        list_controllers = rospy.ServiceProxy(
         '/hsrb/controller_manager/list_controllers',
           controller_manager_msgs.srv.ListControllers)
        running = False
        while running is False:
            rospy.sleep(0.1)
            for c in list_controllers().controller:
                if c.name == 'head_trajectory_controller' and c.state == 'running':
                    running = True

        self._state_sub = rospy.Subscriber("/hsrb/head_trajectory_controller/state",
                JointTrajectoryControllerState, self._state_callback, queue_size=1)

        self._state_received = False
        self._current_pan = 0.0 # Degree
        self._current_tilt = 0.0 # Degree

    def _state_callback(self, state):
        # 首の現在角度を取得
        self._state_received = True
        pan_radian = state.actual.positions[0]
        tilt_radian = state.actual.positions[1]

        self._current_pan = math.degrees(pan_radian)
        self._current_tilt = math.degrees(tilt_radian)

    def state_received(self):
        return self._state_received

    def get_current_pan(self):
        return self._current_pan # Radian

    def get_current_tilt(self):
        return self._current_tilt # Radian

    def set_angle(self, pan_angle, tilt_angle, goal_secs=1.0e-9):
        # 首を指定角度に動かす
        goal = FollowJointTrajectoryGoal()
        goal.trajectory.joint_names = ["head_pan_joint", "head_tilt_joint"]

        panpoint = JointTrajectoryPoint()
        panpoint.positions.append(pan_angle)
        panpoint.positions.append(tilt_angle)
        panpoint.time_from_start = rospy.Duration(goal_secs)
        goal.trajectory.points.append(panpoint)

        self.__client.send_goal(goal)
        self.__client.wait_for_result(rospy.Duration(0.1))
        return self.__client.get_result()

def hook_shutdown():
    # shutdown時に0度へ戻る
    neck.set_angle(math.radians(0), math.radians(0), 3.0)

def main():
    r = rospy.Rate(60)

    rospy.on_shutdown(hook_shutdown)

    # オブジェクト追跡のしきい値
    # 正規化された座標系(px, px)
    THRESH_X = 0.05
    THRESH_Y = 0.05

    # 首の初期角度 Degree
    INITIAL_PAN_ANGLE = 0
    INITIAL_TILT_ANGLE = 0

    # 首の制御角度リミット値 Degree
    #head_pan_joint  回転  z -3.839～1.745[rad]  -220～100[deg]
    #head_tilt_joint 回転 -y -1.570～0.523[rad]  -90～30[deg]
    MAX_PAN_ANGLE   =45 
    MIN_PAN_ANGLE   = -45
    MAX_TILT_ANGLE = 20 
    MIN_TILT_ANGLE = -10

    # 首の制御量
    # 値が大きいほど首を大きく動かす
    OPERATION_GAIN_X = 1.0
    OPERATION_GAIN_Y = 1.0

    # 初期角度に戻る時の制御角度 Degree
    RESET_OPERATION_ANGLE = 3


    while not neck.state_received():
        pass
    pan_angle = neck.get_current_pan()
    tilt_angle = neck.get_current_tilt()

    look_object = False
    detection_timestamp = rospy.Time.now()

    while not rospy.is_shutdown():
        # 正規化された顔の中心座標を取得
        face_position = face_tracker.get_face_position()

        if face_tracker.face_detect():
            detection_timestamp = rospy.Time.now()
            look_object = True
        else:
            lost_time = rospy.Time.now() - detection_timestamp
            # 一定時間顔が見つからない場合は初期角度に戻る
            if lost_time.to_sec() > 3.0:
                look_object = False

        if look_object:
            # 顔が画像中心にくるように首を動かす
            if math.fabs(face_position.x) > THRESH_X:
                pan_angle += -face_position.x * OPERATION_GAIN_X

            if math.fabs(face_position.y) > THRESH_Y:
                tilt_angle += face_position.y * OPERATION_GAIN_Y

            # 首の制御角度を制限する
            if pan_angle > MAX_PAN_ANGLE:
                pan_angle = MAX_PAN_ANGLE
            if pan_angle < MIN_PAN_ANGLE:
                pan_angle = MIN_PAN_ANGLE

            if tilt_angle > MAX_TILT_ANGLE:
                tilt_angle = MAX_TILT_ANGLE
            if tilt_angle < MIN_TILT_ANGLE:
                tilt_angle = MIN_TILT_ANGLE

        else:
            # ゆっくり初期角度へ戻る
            diff_pan_angle = pan_angle - INITIAL_PAN_ANGLE
            if math.fabs(diff_pan_angle) > RESET_OPERATION_ANGLE:
                pan_angle -= math.copysign(RESET_OPERATION_ANGLE, diff_pan_angle)
            else:
                tilt_angle = INITIAL_TILT_ANGLE

            diff_tilt_angle = tilt_angle - INITIAL_TILT_ANGLE
            if math.fabs(diff_tilt_angle) > RESET_OPERATION_ANGLE:
                tilt_angle -= math.copysign(RESET_OPERATION_ANGLE, diff_tilt_angle)
            else:
                tilt_angle = INITIAL_TILT_ANGLE

        neck.set_angle(math.radians(pan_angle), math.radians(tilt_angle))
        r.sleep()


if __name__ == '__main__':
    rospy.init_node("head_camera_tracking")

    neck = HeadPanTilt()
    neck.set_angle(0,0)
    face_tracker = FaceTracker()
#    omni = OmniBase()
#    omni.set_omni(math.radians(90))
#    rospy.sleep(5)

    main()
