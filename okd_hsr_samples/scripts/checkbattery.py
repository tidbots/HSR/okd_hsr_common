#!/usr/bin/env python3
## coding: UTF-8

"""
/hsrb/battery_state
type
/hsrb/battery_state

Classhsrb_interface.battery.Battery(name)
Abstract interface to get battery status.

charge
float

Remaining battery charge [%].

temperature
int

Battery temperature [deg C].

"""
import sys
import os
import datetime
import math
import rospy
from std_msgs.msg import *
from tmc_msgs.msg import *
import hsrb_interface
from hsrb_interface import geometry


def listenCharge():
    m = rospy.wait_for_message('/hsrb/battery_state', BatteryState, timeout=None)
    rospy.loginfo(m.power)

    return m.power


def main():
    rospy.init_node('hsr_checkbattery', anonymous=True)
    rospy.loginfo("HSR check battery status")  

    charge=listenCharge()
    robot = hsrb_interface.Robot()
    tts = robot.get('default_tts')
    rospy.sleep(1)

    

    # Detect robot's language
    if os.environ['LANG'] == 'ja_JP.UTF-8':
        rospy.loginfo(u"バッテリーの残量は" + str(int(charge)) +"パーセントです")
        tts.say(u"バッテリーの残量は" + str(int(charge)) +"パーセントです")

    else:
        rospy.loginfo(u"Battery level is " + str(int(charge)) +u" percent")
        tts.say(u"Battery level is" + str(int(charge)) +u"percent")
    rospy.sleep(3)

if __name__ == '__main__':
    try:
        main()
    except rospy.ROSInterruptException:
        pass

