#!/usr/bin/env python3
# Copyright (C) 2016 Toyota Motor Corporation
# Copyright (C) 2022 Hiroyuki Okada
## coding: UTF-8
from geometry_msgs.msg import Point, PoseStamped, Quaternion
import rospy
import tf.transformations
import math

def main():
    # initialize ROS publisher
    pub = rospy.Publisher('goal', PoseStamped, queue_size=10)

    # wait to establish connection between the navigation interface
    # move_base and navigation_log_recorder node
    while pub.get_num_connections() < 2:
        rospy.sleep(0.1)

   # 移動先の位置、姿勢(x座標、y座標、回転)
    goal_x, goal_y, goal_yaw = 1.0, 1.0, math.radians(90)

    # fill ROS message
    goal = PoseStamped()
    goal.header.stamp = rospy.Time.now()
    goal.header.frame_id = "map"
    goal.pose.position = Point(goal_x, goal_y, 0)
    quat = tf.transformations.quaternion_from_euler(0, 0, goal_yaw)
    goal.pose.orientation = Quaternion(*quat)

    # publish ROS message
    pub.publish(goal)


if __name__ == "__main__":
    rospy.init_node('navigation_topic')
    main()


